#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <sys/mman.h>

#define HEAP_SIZE 1024

int free_heap(void* heap, size_t length) {
    return munmap(heap, size_from_capacity((block_capacity) { .bytes = length }).bytes);
}



//Обычное успешное выделение памяти.
void test_normal_memory_allocation() {
    printf("\n\n%s\n", "Test 1 ---Normal successful memory allocation----");

    void* heap = heap_init(HEAP_SIZE);

    if (heap == NULL){
        err("Test 1 failed. Sorry +_+ \n");
    }
    debug_heap(stdout, heap);

    void *test = _malloc(1024);

    if (!test) {
        err("Test 1 failed. Sorry +_+ \n");
    } else {
        printf("Test 1 passed \n");
    }
    _free(test);
    free_heap(heap,HEAP_SIZE);
}

//Освобождение одного блока из нескольких выделенных
void test_free_block() {

    printf("\n\n%s\n", "Test 2 ---Free one block from several allocated----\n");

    void* heap = heap_init(HEAP_SIZE);

    void* block1 = _malloc(1007);
    void* block2 = _malloc(4000);
    if (!block1 || !block2) {
        printf("Test 2 failed. Sorry +_+\n");
        return;
    }
    _free(block1);
    _free(block2);
    printf("Test 2 passed \n");
    free_heap(heap,HEAP_SIZE);
}

//Освобождение двух блоков из нескольких выделенных.
void test_free_two_blocks() {

    printf("\n\n%s\n", "Test 3 ----Free two blocks from several allocated----");

    void* heap = heap_init(HEAP_SIZE);

    void* block1 = _malloc(1010);
    void* block2 = _malloc(2020);
    void* block3 = _malloc(3030);
    if (!block1 || !block2 || !block3) {
        printf("Test 3 failed. Sorry +_+\n");
        return;
    }
    _free(block1);
    _free(block2);
    printf("Test 3 passed\n");
    free_heap(heap,HEAP_SIZE);

}

//Память закончилась, новый регион памяти расширяет старый.
void test_new_point_after_last(size_t heap_size) {

    printf("\n\n%s\n", "Test 4 ---The memory is over, the new region of memory expands the old---");

    void* heap = heap_init(HEAP_SIZE);

    void* block = _malloc(heap_size + 1);
    if (!block) {
        printf("Test 4 failed. Sorry +_+\n");
        return;
    }
    _free(block);
    printf("Test 4 passed\n");
    free_heap(heap,HEAP_SIZE);
}


/*
 * Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов,
 * новый регион выделяется в другом месте. Тесты должны запускаться из main.c,
 * но могут быть описаны в отдельном (отдельных) файлах.
 * Алгоритм не самый простой, легко ошибиться.
 * Чтобы не тратить времени на отладку, обязательно делайте разбиение на маленькие функции!
 */
void test_new_point_at_new_place(size_t heap_size) {
    printf("\n\n%s\n", "Test 5 ---The Memory run out, new region allocated elsewhere---");

    void* heap = heap_init(HEAP_SIZE);

    (void) mmap(HEAP_START + REGION_MIN_SIZE,
                REGION_MIN_SIZE,
                PROT_READ | PROT_WRITE,
                MAP_PRIVATE | MAP_FIXED,
                -1,
                0);
    void* block = _malloc(heap_size + 1);
    if (!block) {
        printf("Test 5 failed. Sorry +_+\n");
        return;
    }
    _free(block);
    printf("Test 5 passed\n");
    free_heap(heap,HEAP_SIZE);
}



//void* start_init(size_t heap_size) {
//    void* heap = heap_init(heap_size);
//    debug_heap(stdout, heap);
//    return heap;
//}

int main() {
//    printf("%s\n", "----Heap initialization----");
    size_t heap_size = REGION_MIN_SIZE;
//    void* heap = start_init(heap_size);
//    if (!heap) {
//        printf("Failed to init heap");
//        return 0;
//    }

    test_normal_memory_allocation();
    test_free_block();
    test_free_two_blocks();
    test_new_point_after_last(heap_size);
    test_new_point_at_new_place(heap_size);


    //free_heap(heap, heap_size);

    printf("%s\n", "Testing is over.");
    return 0;
}